const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

describe('getClientController', () => {

    beforeEach(function () {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock('../config',{
          config: {}
        });
        mockery.registerMock('../services/redis/redis-service',{
        });
    });

    afterEach(function () {
        mockery.disable();
        mockery.deregisterAll();
    });

    describe('get method', () => {

      it('should get a client by id', async function () {
        const mockRedis = function () {
          this.getData= async(id) => {
            return `{"id":${id}}`
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./get-client-controller");
        controller = new Controller();
        sinon.stub(controller, 'createResponse').returns({code: 'ok', message: {}});
        const id = 1;
        const response = await controller.get(id);
        expect(response.code).to.be.equal("ok");
        expect(response.message).to.be.not.null;
      });

      it('should throw an error when something fails', async function () {
        const mockRedis = function () {
          this.getData= async(id) => {
            throw new Error("internal")
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./get-client-controller");
        controller = new Controller();
        const id = 1;
        try {
          await controller.get(id);
        } catch(e) {
          expect(e.message).to.be.equal("internal");
        }
      });


    });

    describe('createResponse method', () => {

      it('should return a code ok', function () {
        const Controller = require("./get-client-controller");
        controller = new Controller();
        const response = {};
        const finalResponse = controller.createResponse(response);
        expect(finalResponse.code).to.be.equal("ok");
      });

      it('should return a code not found', function () {
        const Controller = require("./get-client-controller");
        controller = new Controller();
        const response = null;
        const finalResponse = controller.createResponse(response);
        expect(finalResponse.code).to.be.equal("not found");
      });
    });

});
