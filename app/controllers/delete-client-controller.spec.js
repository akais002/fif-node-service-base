const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

describe('deleteClientController', () => {

    beforeEach(function () {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock('../config',{
          config: {}
        });
        mockery.registerMock('../services/redis/redis-service',{
        });
    });

    afterEach(function () {
        mockery.disable();
        mockery.deregisterAll();
    });

    describe('delete method', () => {

      it('should delete a client by id', async function () {
        const mockRedis = function () {
          this.deleteData= async(id) => {
            return {
              id
            }
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./delete-client-controller");
        controller = new Controller();
        sinon.stub(controller, 'createResponse').returns({code: 'ok'});
        const client = {id:1};
        const response = await controller.delete(client);
        expect(response.code).to.be.equal("ok");
      });

      it('should throw an error when something fails', async function () {
        const mockRedis = function () {
          this.deleteData= async(id) => {
            throw new Error("internal")
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./delete-client-controller");
        controller = new Controller();
        const client = {id:1};
        try {
          await controller.delete(client);
        } catch(e) {
          expect(e.message).to.be.equal("internal");
        }
      });


    });

    describe('createResponse method', () => {

      it('should return a code ok', function () {
        const Controller = require("./delete-client-controller");
        controller = new Controller();
        const response = {};
        const finalResponse = controller.createResponse(response);
        expect(finalResponse.code).to.be.equal("ok");
      });

      it('should return a code not found', function () {
        const Controller = require("./delete-client-controller");
        controller = new Controller();
        const response = null;
        const finalResponse = controller.createResponse(response);
        expect(finalResponse.code).to.be.equal("not found");
      });
    });

});
