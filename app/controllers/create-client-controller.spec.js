const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

describe('createClientController', () => {

    beforeEach(function () {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock('../config',{
          config: {}
        });
        mockery.registerMock('../services/redis/redis-service',{
        });
    });

    afterEach(function () {
        mockery.disable();
        mockery.deregisterAll();
    });

    describe('create method', () => {

      it('should return the info when the client exists', async function () {
        const Controller = require("./create-client-controller");
        controller = new Controller();
        sinon.stub(controller, 'clientExist').returns(Promise.resolve({id:1}));
        sinon.stub(controller, 'createResponse').returns({code: 'exists',message: null});
        const client = {id:1};
        const response = await controller.create(client);
        expect(response.code).to.be.equal("exists");
        expect(response.message).to.be.null;
      });

      it('should return the info when the client was created', async function () {
        const mockRedis = function () {
          this.setData= async(id, info,ttl) => {
            return {
              id
            }
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        mockery.registerMock('../config',{
          config:{
            redisConfig:{
              ttl:1111
            }
          }
        });
        const Controller = require("./create-client-controller");
        controller = new Controller();
        sinon.stub(controller, 'clientExist').returns(Promise.resolve(null));
        sinon.stub(controller, 'createResponse').returns({code: 'ok',message: {data:1}});
        const client = {id:1};
        const response = await controller.create(client);
        expect(response.code).to.be.equal("ok");
        expect(response.message).to.be.not.null;
      });

      it('should throw an error when something fails', async function () {
        const Controller = require("./create-client-controller");
        controller = new Controller();
        sinon.stub(controller, 'clientExist').throws(new Error('internal'));
        const client = {id:1};
        try {
          await controller.create(client);
        } catch(e) {
          expect(e.message).to.be.equal("internal");
        }
      });


    });

    describe('createResponse method', () => {

      it('should return a code exists', function () {
        const Controller = require("./create-client-controller");
        controller = new Controller();
        const response = {};
        const message = null;
        const finalResponse = controller.createResponse(response, message);
        expect(finalResponse.code).to.be.equal("exists");
        expect(finalResponse.message).to.be.null;
      });

      it('should return a code ok', function () {
        const Controller = require("./create-client-controller");
        controller = new Controller();
        const response = null;
        const message = {data:'data'};
        const finalResponse = controller.createResponse(response, message);
        expect(finalResponse.code).to.be.equal("ok");
        expect(finalResponse.message.data).to.be.equal("data");
      });
    });

    describe('clientExist method', () => {

      it('should return a client', async function () {
        const mockRedis = function () {
          this.getData= async(id) => {
            return {
              id
            }
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./create-client-controller");
        controller = new Controller();
        const response = await controller.clientExist(12);
        expect(response.id).to.be.equal(12);
      });

      it('should throw an error', async function () {
        const mockRedis = function () {
          this.getData= async(id) => {
            throw new Error("internal error")
          }
        }
        mockery.registerMock('../services/redis/redis-service',mockRedis);
        const Controller = require("./create-client-controller");
        controller = new Controller();
        try {
          await controller.clientExist(12);
        } catch(e) {
          expect(e.message).to.be.equal("internal error");
        }
      });

    });



    describe('createClient method', () => {

      it('should return a json with the info of a new client', function () {
        const Controller = require("./create-client-controller");
        controller = new Controller();
        const client = {
          id: "id",
          other: "other"
        }
        const response = controller.createClient(client);
        expect(response.id).to.be.equal("id");
        expect(response.info).to.be.string;
      });

    });

});
