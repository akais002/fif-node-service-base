const mockery = require('mockery');
const chai = require('chai');
const expect = chai.expect;
var sinon = require('sinon');

describe('RedisService', () => {

  let client = null;

  beforeEach(function () {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe("setData method", () => {

    it("should call the connection correctly", async () => {
      mockery.registerMock('./redis-connection', {
        checkInitConnection: () => {
          return Promise.resolve()
        }
      });
      try {
        require('./init-redis-connection');
      } catch (e) {

      }
    });

    it("should call the process.exit", async () => {
      mockery.registerMock('./redis-connection', {
        checkInitConnection: () => {
          return Promise.reject("error")
        }
      });
      try {
        process.on('exit', function(code) {
          expect(code).to.be.equal(0);
        });
        require('./init-redis-connection');

      } catch (e) {

      }
    });

  });

});
